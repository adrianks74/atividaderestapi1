package com.adrian.RESTApiTest.Controllers;


import java.util.List;

import org.hibernate.criterion.Example;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.adrian.RESTApiTest.Models.Candidate;
import com.adrian.RESTApiTest.Models.CityData;
import com.adrian.RESTApiTest.Repositories.CandidateRepository;
import com.adrian.RESTApiTest.Repositories.CityDataRepository;


////Controlador para dados de populacao de cidade
////Permite Busca de todos anos e população GET (/)
////Permite Busca da população de um ano GET (/{year})
////Permite inserção de dados POST (/)
////Permite edição de dados PUT (/)
////Permite remoção de dados DELETE (/)

@RestController
@RequestMapping("/citydata")
public class CityDataController
{
	@Autowired
	private CityDataRepository cityDataRepository;
	
	// ENDPOINTS
	@GetMapping("/")
	public List<CityData> GetAll()
	{
		return cityDataRepository.findAll();
	}
	
	@GetMapping("/{year}")
	public List<CityData> GetByYear(@PathVariable int year)
	{
		return cityDataRepository.findByYear( year );
	}
	
	@PostMapping("/")
	public ResponseEntity PostYearData(@RequestBody CityData cityData)
	{
		// Invalid year
		if( cityData.getYear() < 1500 && cityData.getYear() > 300000 )
		{
			return new ResponseEntity( HttpStatus.BAD_REQUEST );
		}
		
		// City already exists
		if( !GetByYear(cityData.getYear()).isEmpty() )
		{
			return new ResponseEntity( HttpStatus.BAD_REQUEST );
		}
		
		// SUCCESS
		cityDataRepository.save(cityData);

		return new ResponseEntity( cityData, HttpStatus.BAD_REQUEST );
		
	}
	
	@PutMapping("/")
	public ResponseEntity UpdatePopulation(@RequestBody CityData cityD)
	{
		CityData cd = cityDataRepository.getOne(cityD.getId());
		
		if(cd == null) return new ResponseEntity( HttpStatus.BAD_REQUEST );
		
		cd.setPopulation( cityD.getPopulation() );
		cityDataRepository.save(cd);
		
		return new ResponseEntity( cd, HttpStatus.OK );
	}
	
	@DeleteMapping("/")
	public ResponseEntity Removeyear(@RequestBody CityData cityD)
	{
		cityDataRepository.deleteById(cityD.getId());
		
		return new ResponseEntity( HttpStatus.OK );
	}
	
	
}

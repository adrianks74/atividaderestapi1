package com.adrian.RESTApiTest.Controllers;

import com.adrian.RESTApiTest.Models.APIDate;

public class APISolver
{
	public static APIDate GetNow( )
	{
		return new APIDate();
	}
	
	public static float GetCelsius( float f )
	{
		return (f-32)*(5f/9f);
	}
	
	public static boolean IsEven( int n )
	{
		return n % 2 == 0;
	}
}

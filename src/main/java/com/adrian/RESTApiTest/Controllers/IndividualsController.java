package com.adrian.RESTApiTest.Controllers;

import java.util.List;

import org.hibernate.criterion.Example;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.adrian.RESTApiTest.Models.CityData;
import com.adrian.RESTApiTest.Models.Individual;
import com.adrian.RESTApiTest.Repositories.IndividualRepository;


//// Controlador para individuos (professores e estudantes)
//// Permite Busca de todos individuos GET (/)
//// Permite Busca de professores (/professors)
//// Permite Busca de estudantes  (/students)
//// Permite inserção de individuo POST (/)
//// Permite edição de individuo PUT (/)
//// Permite remoção de individuo DELETE (/)

@RestController
@RequestMapping("/individuals")
public class IndividualsController
{
	@Autowired
	private IndividualRepository individualRepository;
	
	// ENDPOINTS
	@GetMapping("/")
	public List<Individual> GetAll()
	{
		return individualRepository.findAll();
	}
	
	@GetMapping("/professors")
	public List<Individual> GetAllProfessors()
	{
		return individualRepository.findByIndType( Individual.IndType.Professor );
	}
	
	@GetMapping("/students")
	public List<Individual> GetAllStudents()
	{
		return individualRepository.findByIndType( Individual.IndType.Student );
	}
	
	@PostMapping("/")
	public ResponseEntity PostIndividual(@RequestBody Individual ind)
	{
		// SAVE
		individualRepository.save(ind);

		return new ResponseEntity( ind, HttpStatus.OK );
	}
	
	@PutMapping("/")
	public ResponseEntity PutIndividual(@RequestBody Individual ind)
	{
		Individual individual = individualRepository.getOne( ind.getId());
		// ERROR
		if(individual == null)
		{
			return new ResponseEntity( HttpStatus.BAD_REQUEST );
		}
		// SAVE
		else
		{
			individual.setName(ind.getName());
			individual.setIndType(ind.getIndType());
			
			individualRepository.save(individual);
			
			return new ResponseEntity( individual, HttpStatus.OK );
		}
	}
	
	@DeleteMapping("/")
	public ResponseEntity RemoveIndividual(@RequestBody Individual ind)
	{
		Individual individual = individualRepository.getOne( ind.getId());
		// ERROR
		if(individual == null)
		{
			return new ResponseEntity( HttpStatus.BAD_REQUEST );
		}
		// SAVE
		else
		{
			individualRepository.deleteById( ind.getId() );
			return new ResponseEntity( HttpStatus.OK );
		}
	}
}

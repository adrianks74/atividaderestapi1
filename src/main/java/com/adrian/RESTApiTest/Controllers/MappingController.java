package com.adrian.RESTApiTest.Controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.adrian.RESTApiTest.Models.APIDate;
import com.adrian.RESTApiTest.Models.Candidate;

/// API generica para buscar Data atual, conversão de fahrenheit para celsius, e descobrir se um numero é par ou impar
@RestController
@RequestMapping("/")
public class MappingController
{
	
	// 1, 2, 3
	@GetMapping("/getdate")
	public APIDate GetDate()
	{
		return APISolver.GetNow();
	}
	
	@GetMapping("/fahrenheittocelsius/{fahrenheit}")
	public float GetCelsius(@PathVariable float fahrenheit)
	{
		return APISolver.GetCelsius(fahrenheit);
	}
	
	@GetMapping("/iseven/{number}")
	public boolean GetEven(@PathVariable int number)
	{
		return APISolver.IsEven(number);
	}
}

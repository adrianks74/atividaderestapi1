package com.adrian.RESTApiTest.Controllers;

import java.util.List;

import org.hibernate.criterion.Example;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.adrian.RESTApiTest.Models.Candidate;
import com.adrian.RESTApiTest.Repositories.CandidateRepository;

////Controlador para eleicao
////Permite Busca de todos candidatos com numero de votos GET (/)
////Permite voto em um candidato POST (/vote/{number})
////Permite busca de numero de votos de um candidato GET ("/votecount/{number}")

@RestController
@RequestMapping("/election")
public class ElectionController
{
	@Autowired
	private CandidateRepository candidates;
	
	private boolean started = false;
	
	
	// ENDPOINTS
	@GetMapping("/")
	public List<Candidate> GetAll()
	{
		EnsureFilledCandidates();
		return candidates.findAll();
	}
	
	@PostMapping("/vote/{number}")
	public boolean PostVote(@PathVariable int number)
	{
		EnsureFilledCandidates();
		
		Candidate c = GetByBallotNumber(number);
		
		if(c != null)
		{
			c.AddVote();
			candidates.save(c);
			
			return true;
		}
		
		return false;
	}
	
	@GetMapping("/votecount/{number}")
	public ResponseEntity GetVoteCount(@PathVariable int number)
	{
		EnsureFilledCandidates();
		
		Candidate c = GetByBallotNumber(number);
		
		// FOUND, RESPOND WITH VOTECOUNT
		if(c != null)
		{
			return new ResponseEntity( c.getVoteCount(), HttpStatus.OK ) ;
		}
		// ERROR, RESPOND WITH STATUS CODE
		else
		{
			return new ResponseEntity( HttpStatus.BAD_REQUEST );
		}
	}
	
	
	//METHODS
	
	void EnsureFilledCandidates()
	{
		if(started) return;
		
		candidates.save( Candidate.NewCandidate("Aline", 11));
		candidates.save( Candidate.NewCandidate("Bruno", 25));
		candidates.save( Candidate.NewCandidate("Cheron", 19));
		candidates.save( Candidate.NewCandidate("Dadinho do gas", 32));
		candidates.save( Candidate.NewCandidate("Tia Suzi", 30));
		
		started = true;
	}
	
	Candidate GetByBallotNumber(int number)
	{
		List<Candidate> all = candidates.findByNumber(number);
		
		// 
		if(all.size() > 0) return all.get(0);

		/*for( Candidate c : all )
			if( c.getNumber() == number )
				return c;*/
		
		return null;	
	}

	
}

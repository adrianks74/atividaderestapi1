package com.adrian.RESTApiTest.Models;

import java.time.*;

public class APIDate
{
	private  int year, month, day, hour, minute, second;
	
	// Creates APIDate with values from Now
	public APIDate()
	{
		LocalDateTime now = LocalDateTime.now();
		
		day =    now.getDayOfMonth();
		month =  now.getMonthValue();
		year =   now.getYear();
		hour =   now.getHour();
		second = now.getSecond();
		minute = now.getMinute();
	}
		
	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		this.day = day;
	}

	public int getHour() {
		return hour;
	}

	public void setHour(int hour) {
		this.hour = hour;
	}

	public int getMinute() {
		return minute;
	}

	public void setMinute(int minute) {
		this.minute = minute;
	}

	public int getSecond() {
		return second;
	}

	public void setSecond(int second) {
		this.second = second;
	}

	
	
}

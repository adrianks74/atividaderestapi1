package com.adrian.RESTApiTest.Models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Candidate
{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(nullable = false)
	private String name;
	
	@Column(nullable = false)
	private int number;
	
	@Column(nullable = false)
	private int voteCount;
	
	// "Constructor"
	public static Candidate NewCandidate( String nam, int num )
	{
		Candidate c = new Candidate();
		c.name = nam;
		c.number = num;
		c.voteCount = 0;
		
		return c;
	}
	
	public Candidate()
	{
		
	}
	
	public void AddVote()
	{
		voteCount++;
	}
	
	
	// Getters Setters
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public int getVoteCount() {
		return voteCount;
	}

	public void setVoteCount(int voteCount) {
		this.voteCount = voteCount;
	}
}

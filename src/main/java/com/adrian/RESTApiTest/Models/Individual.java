package com.adrian.RESTApiTest.Models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Entity
public class Individual
{
	public enum IndType
	{
		Professor, Student
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public IndType getIndType() {
		return indType;
	}

	public void setIndType(IndType indType) {
		this.indType = indType;
	}

	@Column(nullable = false)
	private String name;
	
	@Enumerated(EnumType.ORDINAL)
	@Column(nullable = false)
	private IndType indType;
	
	// Getters setters

}

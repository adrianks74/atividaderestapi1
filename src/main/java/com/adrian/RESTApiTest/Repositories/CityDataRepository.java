package com.adrian.RESTApiTest.Repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.adrian.RESTApiTest.Models.Candidate;
import com.adrian.RESTApiTest.Models.CityData;

@Repository
public interface CityDataRepository extends JpaRepository<CityData, Long>
{
	List<CityData> findByYear( int year );
}
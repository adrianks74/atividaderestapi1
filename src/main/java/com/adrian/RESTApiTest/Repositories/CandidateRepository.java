package com.adrian.RESTApiTest.Repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.adrian.RESTApiTest.Models.Candidate;


@Repository
public interface CandidateRepository extends JpaRepository<Candidate, Long>
{
	List<Candidate> findByNumber( int number );
}

package com.adrian.RESTApiTest.Repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.adrian.RESTApiTest.Models.Candidate;
import com.adrian.RESTApiTest.Models.CityData;
import com.adrian.RESTApiTest.Models.Individual;

@Repository
public interface IndividualRepository extends JpaRepository<Individual, Long>
{
	List<Individual> findByIndType( Individual.IndType t );
}
